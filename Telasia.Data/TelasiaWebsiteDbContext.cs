﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Telasia.Data.Models;
using Telasia.Data.Models.Identity;

namespace Telasia.Data
{
    public class TelasiaWebsiteDbContext : DbContext
    {
        public TelasiaWebsiteDbContext(DbContextOptions options) : base(options) {
        }

        public DbSet<News> News { get; set; }
        public DbSet<PasswordResetToken> PasswordResetTokens { get; set; }
    }
}
