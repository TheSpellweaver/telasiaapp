﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Telasia.Data.Models.Identity
{
    public class PasswordResetToken
    {
        [Key]
        public Guid Id { get; set; }
        
        public Guid PlayerId { get; set; }

        public DateTime ExpirationDate { get; set; }

        public bool IsValid { get; set; }
    }
}
