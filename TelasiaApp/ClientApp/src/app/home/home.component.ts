import { Component } from '@angular/core';
import { NewsService } from '../_services/news.service';
import News from '../_models/News';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.css'],
})
export class HomeComponent {
  public news: News[];

  constructor(newsService: NewsService) {
    newsService.getNews(1, 10).subscribe(result => {
      this.news = result;
    }, error => console.error(error));
  }
}
