import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<any>;
  private apiUrl: string;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient, @Inject('BASE_URL')  baseUrl: string) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    // this.apiUrl = baseUrl;
    this.apiUrl = environment.telasiaWebUrl;
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${this.apiUrl}/users/authenticate`, { username, password })
      .pipe(map(user => {
        console.log(user);
        // Store user details and token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  register(username: string, password: string, email: string) {
    return this.http.post<any>(`${this.apiUrl}/users/register`, { username, password, email });
  }

  logout() {
    // Remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  validateUsername(username: string) {
    return this.http.get<any>(`${this.apiUrl}/users/validate-username/${username}`)
      .pipe(res => res);
  }

}
