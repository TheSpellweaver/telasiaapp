import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import News from '../_models/News';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = environment.telasiaWebUrl;
  }

  addNews() {

  }

  getNews(page, count) {
    console.log(this.baseUrl, "baseUrl");
    return this
      .http.get<News[]>(`api/News/?page=${page}&count=${count}`);
  }
}
