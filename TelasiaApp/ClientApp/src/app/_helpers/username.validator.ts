import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AuthenticationService } from '../_services/authentication.service';

@Injectable({
    providedIn: 'root',
})
export class UsernameValidator {

  debouncer: any;

  constructor(public authenticationService: AuthenticationService){

  }

  checkUsername(control: FormControl): any {

    clearTimeout(this.debouncer);

    return new Promise(resolve => {

      this.debouncer = setTimeout(() => {

        this.authenticationService.validateUsername(control.value).subscribe((res) => {
            
            console.log(res);
          if(res == true){
            resolve(null);
          } else {
              resolve({'usernameInUse': true});
          }
        }, (err) => {
            resolve({'usernameInUse': true});
        });

      }, 1000);      

    });
  }

}