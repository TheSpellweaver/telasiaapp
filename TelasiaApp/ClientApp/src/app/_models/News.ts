export default class News {
    title: string;
    userId: number;
    username: string;
    avatarUri: string;
    imageUri: string;
    content: string;
    timestamp: string;
}