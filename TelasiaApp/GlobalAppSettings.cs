﻿namespace TelasiaApp
{
    internal class GlobalAppSettings
    {
        public string GameApiUrl { get; set; }
        public double PasswordResetExpiration { get; set; }

        public static GlobalAppSettings Instance =
                        new GlobalAppSettings();

        public GlobalAppSettings()
        {
            Instance = this;
        }
    }
}