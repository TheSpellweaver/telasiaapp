﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace TelasiaApp.Models.Response
{
    [DataContract]
    public class IntersectUserResponse
    {
        [DataMember(Name = "Message")]
        public string Message { get; set; }
        public System.Net.HttpStatusCode Result { get; set; }
    }
}
