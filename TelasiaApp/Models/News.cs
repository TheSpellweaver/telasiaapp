﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Telasia.Data
{
    public class News
    {
        [Key]
        public Guid Id { get; set; }
        public int UserId { get; set; }
        public string AvatarUri { get; set; }
        public string ImageUri { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Shown { get; set; }
    }
}
