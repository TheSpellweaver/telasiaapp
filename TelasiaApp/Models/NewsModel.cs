﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Telasia.Data;

namespace TelasiaApp.Models
{
    public class NewsModel
    {
        public Guid? Id { get; set; }
        public string Title { get; set; }
        public int? UserId { get; set; }
        public string Username  { get; set; }
        public string AvatarUri { get; set; }
        public string ImageUri  { get; set; }
        public string Content  { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
