﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace TelasiaApp.Models
{
    [DataContract]
    public class UserModel
    {
        [DataMember(Name = "Id")]
        public Guid? Id { get; set; }

        [DataMember(Name = "Name")]
        public string Username { get; set; }

        [DataMember(Name = "Email")]
        public string Email { get; set; }

    }
}
