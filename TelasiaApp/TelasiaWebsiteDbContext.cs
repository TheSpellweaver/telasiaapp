﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Telasia.Data
{
    public class TelasiaWebsiteDbContext : DbContext
    {
        public TelasiaWebsiteDbContext(DbContextOptions options) : base(options) { }

        public DbSet<News> News { get; set; }
    }
}
