﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Telasia.Data;
using Telasia.Data.Models.Identity;
using TelasiaApp.Helpers;
using TelasiaApp.Models;

namespace TelasiaApp.Controllers
{
    [Route("api/recoverAccount")]
    [ApiController]
    public class AccountRecoveryController : ControllerBase
    {
        private IntersectUserManagerProvider userManager;
        private static TelasiaWebsiteDbContext _context;

        public AccountRecoveryController(IntersectUserManagerProvider userManager, TelasiaWebsiteDbContext context)
        {
            this.userManager = userManager;
            _context = context;
        }

        [Route("requestPassword")]
        public IActionResult RequestPasswordRecovery([FromBody] UserCredentialsModel credentials)
        {
            var user = userManager.GetUserByCredentials(credentials);

            try
            {
                if (user != null && user.Id != null)
                {
                    var expirationTime = GlobalAppSettings.Instance.PasswordResetExpiration;
                    // Generate a password reset token
                    var token = _context.PasswordResetTokens.Add(new PasswordResetToken
                    {
                        IsValid = true,
                        PlayerId = (Guid)user.Id,
                        ExpirationDate = DateTime.Now.AddMinutes(expirationTime)
                    });
                    _context.SaveChanges();

                    // Send email to the user with the url+token
                    var sentUrl = token.Entity.Id;
                }
                else
                {
                    // Send email to the user saying account wasn't found
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }

        }

        [Route("changePassword")]
        public IActionResult ChangePassword([FromBody]UserCredentialsModel model)
        {
            var passwordToken = _context.PasswordResetTokens.Find(model.Id);
            
            if (passwordToken == null || passwordToken.IsValid == false || passwordToken.ExpirationDate < DateTime.Now)
            {
                return BadRequest("Link is invalid.");
            }
            else
            {
                // Send request to Intersect for a change password
                userManager.ChangePasswordForUser(passwordToken.PlayerId, model.Password);
                return Ok();
            }
        }
    }
}