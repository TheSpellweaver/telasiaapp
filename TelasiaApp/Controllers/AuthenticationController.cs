﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TelasiaApp.Models;
using RestSharp;
using RestSharp.Authenticators;
using TelasiaApp.Helpers;
using TelasiaApp.Models.Response;

namespace TelasiaApp.Controllers
{
    [ApiController]
    [Route("users")]
    public class AuthenticationController : ControllerBase
    {
        private IntersectUserManagerProvider userManager;
        public AuthenticationController(IntersectUserManagerProvider userManager)
        {
            this.userManager = userManager;
        }
        //[HttpPost]
        //[Route("authenticate")]
        //public IActionResult Login()
        //{
        //    var token = "mr2gvj29igj4392ij23gij2g";

        //    var user = new
        //    {
        //        id = 1234,
        //        username = "User 1",
        //        firstName = "Name",
        //        lastName = "Lastname",
        //        token
        //    };

        //    return Ok(user);
        //}

        [HttpGet]
        [Route("validate-username/{username}")]
        public IActionResult ValidateUsername([FromRoute]string username)
        {
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;
            //if (username == "1")
            //{
                return Ok(true);
            //}
            //else
            //{
            //    return Ok(false);
            //}

            //return BadRequest();
        }

        [HttpPost]
        [Route("authenticate")]
        public IActionResult Login([FromBody]UserCredentialsModel credentials)
        {
            if (userManager.ValidatePassword(credentials))
            {
                var user = userManager.GetUserByCredentials(credentials);

                if (user != null)
                {
                    return Ok(user);
                }
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] UserCredentialsModel credentials)
        {
            try
            {
                var response = userManager.RegisterUser(credentials);
                if (response.Result.Equals(System.Net.HttpStatusCode.OK))
                {
                    return await Task.FromResult(Ok());
                }
                else
                {
                    return BadRequest(response.Message);
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return new StatusCodeResult(500);
        }
    }
}