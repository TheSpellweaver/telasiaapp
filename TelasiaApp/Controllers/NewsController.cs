﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telasia.Data;
using Telasia.Data.Models;
using TelasiaApp.Models;

namespace TelasiaApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private static TelasiaWebsiteDbContext _context;
        private const int DefaultPageRecordCount = 10;

        public NewsController(TelasiaWebsiteDbContext context)
        {
            _context = context;
        }

        // GET: api/News
        [HttpGet]
        public IActionResult Get(int? page, int? count)
        {
            var retVal = new List<NewsModel>();
            var takePage = page ?? 1;
            var takeCount = count ?? DefaultPageRecordCount;

            var data = _context.News
                    .Skip((takePage - 1) * takeCount)
                    .Take(takeCount)
                    .ToList();

            retVal = data.Select(x => new NewsModel()
            {
                Id = x.Id,
                UserId = x.UserId,
                Username = "DefaultUsername",
                Title = x.Title,
                Content = x.Content,
                ImageUri = x.ImageUri,
                AvatarUri = x.AvatarUri,
                Timestamp = x.CreatedOn
            }).ToList();

            return Ok(retVal);
        }

        // GET: api/News/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var guid = Guid.Parse(id.ToString());

            var data = _context.News
                .FirstOrDefault(x => x.Id == guid);
            return Ok(data);
        }

        // POST: api/News
        [HttpPost]
        public IActionResult Post([FromBody] NewsModel news)
        {
            var newNews = new News()
            {
                Title = news.Title,
                Content = news.Content,
                ImageUri = news.ImageUri,
                CreatedOn = DateTime.Now,
                Shown = true
            };
            try
            {
                _context.News.Add(newNews);
                _context.SaveChanges();
                return Ok(newNews);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // PUT: api/News/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
