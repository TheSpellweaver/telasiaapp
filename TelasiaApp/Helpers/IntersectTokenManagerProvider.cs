﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace TelasiaApp
{
    public class IntersectTokenManagerProvider
    {
        private string Token { get; set; }
        private DateTime TokenValidUntil { get; set; }
        private string RefreshToken { get; set; }

        private string User { get; set; }
        private string Password { get; set; }

        public IntersectTokenManagerProvider(string user, string secret)
        {
            using (var sha = SHA256.Create())
            {
                var bytes = sha.ComputeHash(Encoding.UTF8.GetBytes(secret));
                Password = bytes.Aggregate("", (x, y) => $"{x}{y.ToString("x2")}").ToUpper();
                User = user;
            }
            RequestNewToken();
        }

        public string GetToken()
        {
            if (TokenValidUntil < DateTime.Now)
            {
                //RequestNewToken();
                RefreshCurrentToken();
            }

            return "bearer " + Token;
        }

        private void RequestNewToken()
        {
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;

            var client = new RestClient(baseUrl);
            var request = new RestRequest($"/api/oauth/token", Method.POST);
            request.AddJsonBody(new
            {
                grant_type = "password",
                username = User,
                password = Password
            });

            try
            {
                var response = client.Execute(request);
                var content = response.Content;
                var tokenResponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(content);

                if (response.IsSuccessful)
                {
                    Token = tokenResponse.First(x => x.Key == "access_token").Value;
                    TokenValidUntil = DateTime.Now.AddSeconds(tokenResponse.First(x => x.Key == "expires_in").Value);
                    RefreshToken = tokenResponse.First(x => x.Key == "refresh_token").Value;
                }
                else
                {
                    // Error getting token
                }
            }
            catch
            {
                // Log an error
                Token = null;
                TokenValidUntil = DateTime.Now;
                RefreshToken = null;
            }
        }

        private void RefreshCurrentToken()
        {
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;

            var client = new RestClient(baseUrl);
            var request = new RestRequest($"/api/oauth/token", Method.POST);
            request.AddJsonBody(new
            {
                grant_type = "refresh_token",
                refresh_token = RefreshToken
            });

            try
            {
                var response = client.Execute(request);
                var content = response.Content;
                var tokenResponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(content);

                if (response.IsSuccessful)
                {
                    Token = tokenResponse.First(x => x.Key == "access_token").Value;
                    TokenValidUntil = DateTime.Now.AddSeconds(tokenResponse.First(x => x.Key == "expires_in").Value);
                    RefreshToken = tokenResponse.First(x => x.Key == "refresh_token").Value;
                }
                else
                {
                    RequestNewToken();
                }
            }
            catch
            {
                // Log an error
                Token = null;
                TokenValidUntil = DateTime.Now;
                RefreshToken = null;
            }
        }
    }
}