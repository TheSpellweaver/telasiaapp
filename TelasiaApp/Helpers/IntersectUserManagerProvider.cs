﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TelasiaApp.Models;
using TelasiaApp.Models.Response;

namespace TelasiaApp.Helpers
{
    public class IntersectUserManagerProvider
    {
        private IntersectTokenManagerProvider tokenManager;

        public IntersectUserManagerProvider(IntersectTokenManagerProvider tokenManager)
        {
            this.tokenManager = tokenManager;
        }

        public bool ValidatePassword(UserCredentialsModel credentials)
        {
            var retVal = new UserModel();
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;
            string encodedPassword = EncryptPassword(credentials.Password);

            var client = new RestClient(baseUrl);
            var validateRequest = new RestRequest($"/api/v1/users/{credentials.Username}/password/validate", Method.POST);
            validateRequest.AddHeader("authorization", tokenManager.GetToken());
            validateRequest.AddJsonBody(new
            {
                password = encodedPassword
            });

            try
            {
                //var response = http.Post($"{baseUrl}/api/v1/users/{credentials.Username}/password/validate", body);
                var response = client.Execute(validateRequest);
                var content = response.Content;
                //var user = JsonConvert.DeserializeObject(content);

                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            return false;
        }

        public UserModel GetUserByCredentials(UserCredentialsModel credentials)
        {
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;
            var client = new RestClient(baseUrl);

            var userRequest = new RestRequest($"/api/v1/users/{credentials.Username}", Method.GET);
            userRequest.AddHeader("authorization", tokenManager.GetToken());

            var response = client.Execute(userRequest);
            var content = response.Content;
            var user = JsonConvert.DeserializeObject<UserModel>(content);
            
            return user;
        }

        public UserModel GetUserById(Guid id)
        {
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;
            var client = new RestClient(baseUrl);

            var userRequest = new RestRequest($"/api/v1/users/{id}", Method.GET);
            userRequest.AddHeader("authorization", tokenManager.GetToken());

            var response = client.Execute(userRequest);
            var content = response.Content;
            var user = JsonConvert.DeserializeObject<UserModel>(content);

            return user;
        }

        internal void ChangePasswordForUser(Guid userId, string password)
        {
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;
            var client = new RestClient(baseUrl);

            var request = new RestRequest($"/api/v1/users/{userId}/manage/email/change", Method.POST);
            request.AddHeader("authorization", tokenManager.GetToken());
            request.AddJsonBody(new
            {
                new = EncryptPassword(password)
            });

            var response = client.Execute(request);
            var content = response.Content;
            var user = JsonConvert.DeserializeObject<UserModel>(content);

            return user;
        }

        public IntersectUserResponse RegisterUser(UserCredentialsModel credentials)
        {
            IntersectUserResponse retVal = new IntersectUserResponse();
            var baseUrl = GlobalAppSettings.Instance.GameApiUrl;
            string encodedPassword = EncryptPassword(credentials.Password);

            var client = new RestClient(baseUrl);
            var request = new RestRequest($"/api/v1/users/register", Method.POST);
            request.AddHeader("authorization", tokenManager.GetToken());
            request.AddJsonBody(new
            {
                username = credentials.Username,
                email = credentials.Email,
                password = encodedPassword
            });

            try
            {
                //var response = http.Post($"{baseUrl}/api/v1/users/{credentials.Username}/password/validate", body);
                var response = client.Execute(request);
                var content = response.Content;
                var data = JsonConvert.DeserializeObject<IntersectUserResponse>(content);

                if (response.IsSuccessful)
                {
                    retVal.Result = System.Net.HttpStatusCode.OK;
                }
                else
                {
                    retVal.Message = data.Message;
                    retVal.Result = response.StatusCode;
                }
            }
            catch (Exception ex)
            {
                retVal.Message = ex.Message;
                retVal.Result = System.Net.HttpStatusCode.InternalServerError;
            }
            return retVal;
        }

        private string EncryptPassword(string password)
        {
            string encodedPassword = null;

            using (var sha = SHA256.Create())
            {
                var bytes = sha.ComputeHash(Encoding.UTF8.GetBytes(password));
                encodedPassword = bytes.Aggregate("", (x, y) => $"{x}{y.ToString("x2")}");
            }
            return encodedPassword.ToUpper();
        }
    }
}
